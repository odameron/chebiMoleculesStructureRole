# ChebiMoleculesStructureRole


Study of the relation between molecules' structure and role in the [ChEBI ontology](https://www.ebi.ac.uk/chebi/).
